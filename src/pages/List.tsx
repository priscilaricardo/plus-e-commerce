import { IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonList, IonMenuButton, IonTitle, IonToolbar } from '@ionic/react';
import { americanFootball, basketball, beer, bluetooth, boat, build, flask, football, paperPlane, wifi, woman } from 'ionicons/icons';
import React from 'react';

const ListPage: React.FC = () => {
  return (
    <>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Novidades</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <ListItems />
      </IonContent>
    </>
  );
};

const ListItems = () => {
  const icons = [
    woman,
    wifi,
    beer,
    football,
    basketball,
    paperPlane,
    americanFootball,
    boat,
    bluetooth,
    build
  ];

  //const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => {
    return (
      // <IonItem key={x}>
      <IonList>     
      <IonItem href="/list/womanclothes">
        {/* <IonIcon icon={icons[x - 1]} slot="start" /> */}
        <IonIcon icon={icons[0]} slot="start" />
        {/* Item {x} */}
        Moda Feminina
        <div className="item-note" slot="end">
          {/* This is item # {x} */}
          Clique para visitar
        </div>
      </IonItem>
      </IonList>
    );
  //});

  //return <IonList>{items}</IonList>;
};

export default ListPage;
