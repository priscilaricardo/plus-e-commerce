import { IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonMenuButton, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import React from 'react';
import './WomanClothes.css';

const ListCardsWomanClothes: React.FC = () => {
  return (
    <>
      <IonHeader>
          <IonTitle className="centered">Moda Feminina</IonTitle>
      </IonHeader>

      <IonContent>
        <ListCards />
      </IonContent>
    </>
  );
};

const ListCards: React.FunctionComponent = () => {
  return (
    <>
    <IonList>
      <IonItem href="/home">
      <IonGrid>
      <IonRow>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/vestido-midi-rose-com-babados-e-mangas-amplas_332990_600_1.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
          <IonCard>
          <img src="/assets/woman/BLUSA-COM-LACO-04.02.044502401.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/loja-de-roupas-femininas-online-regata-e-calca-verde-clara.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/roupas-femininas-calca-de-sarja-alfaiataria-preta.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/roupas-femininas-essenciais-blog-a-melhor-escolha6.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
          <IonCard>
          <img src="/assets/woman/saia_e_tenis.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/024068_0035_1-VESTIDO-MARAU-ALCA.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
        <IonCol size="3">
        <IonCard>
          <img src="/assets/woman/saia-peixe.jpg"></img>
        <IonCardHeader>
            <IonCardSubtitle>Card Subtitle</IonCardSubtitle>
            <IonCardTitle>Card Title</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            Keep close to Nature's heart... and break clear away, once in awhile,
            and climb a mountain or spend a week in the woods. Wash your spirit clean.
      </IonCardContent>
        </IonCard>
        </IonCol>
      </IonRow>
      </IonGrid>
      </IonItem>
    </IonList>
    </>
  );
};

export default ListCardsWomanClothes;