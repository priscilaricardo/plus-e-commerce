import {
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenuButton,
  IonTitle,
  IonToolbar
  } from '@ionic/react';
import { book, build, colorFill, grid } from 'ionicons/icons';
import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css';
import './../theme.css';

const HomePage: React.FunctionComponent = () => {
  return (
    <>
      <IonHeader>
        <IonToolbar color='medium'>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Finesse Vestir</IonTitle>
          <IonButtons slot="end">
          <Link to="/cart"><i className="material-icons">shopping_cart</i></Link>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard>
          <img src="/assets/roupas.jpg" alt="" width="150" height="340"/>
          <IonCardHeader>
            <IonCardSubtitle>Home page</IonCardSubtitle>
            <IonCardTitle>Bem vindo ao seu site de roupas on line</IonCardTitle>
          </IonCardHeader>
          <IonCardContent>
            <p>
              Aqui você escontra roupas lindas e de ótima qualidade.
            </p>
          </IonCardContent>
        </IonCard>

        <IonList lines="none">
          <IonListHeader>
            <IonLabel>Resources</IonLabel>
          </IonListHeader>
          <IonItem href="https://ionicframework.com/docs/" target="_blank">
            <IonIcon slot="start" color="medium" icon={book} />
            <IonLabel>Ionic Documentation</IonLabel>
          </IonItem>
          <IonItem href="https://ionicframework.com/docs/building/scaffolding" target="_blank">
            <IonIcon slot="start" color="medium" icon={build} />
            <IonLabel>Scaffold Out Your App</IonLabel>
          </IonItem>
          <IonItem href="https://ionicframework.com/docs/layout/structure" target="_blank">
            <IonIcon slot="start" color="medium" icon={grid} />
            <IonLabel>Change Your App Layout</IonLabel>
          </IonItem>
          <IonItem href="https://ionicframework.com/docs/theming/basics" target="_blank">
            <IonIcon slot="start" color="medium" icon={colorFill} />
            <IonLabel>Theme Your App</IonLabel>
          </IonItem>
        </IonList>
      </IonContent>
    </>
  );
};

export default HomePage;
